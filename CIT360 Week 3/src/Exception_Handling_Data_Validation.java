/*
    * A.
    * Create program that gathers input of 2 numbers from the keyboard
    * in its main method and calls the method below to calculate
    *
    * B.
    * Write a method that divides 2 numbers and returns the result
    * Make sure it includes a throws clause if trying to divide by 0
    *
    * C.
    * In main, use exception handling to catch the most specific exception possible.
    * Display descriptive message in the catch to explain the problem.
    * Give user second chance for data entry
    * Display a message in the final statement
    *
    * D.
    * Run and show output when no exception
    * Run and show output when 0 is used as denominator
    *
    * E.
    * Rewrite data collection so that it uses data validation
    * to ensure an error doesn't happen.
    * If the user enters 0 as the denominator, display a message and
    * ask offer a second chance.
    *
    * F.
    * Run and show output when no exception
    * Run and show output when 0 is used as denominator
    *
 */
import java.util.*;

public class Exception_Handling_Data_Validation {

    static float division(float numerator, float denominator){
        return numerator / denominator;
    }

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        boolean test = false;
        float numerator = 1;
        float denominator = 1;
        float answer;

        System.out.println("Enter two numbers. The first (numerator) will be divided by the second (denominator).");

        //input numerator
        while (test == false){
            test = true;
            input = new Scanner(System.in);
            System.out.print("Enter the numerator: ");
            try {
                numerator = input.nextFloat();
            } catch (Exception e) {
                test = false;
                System.out.println("Invalid input: not a number.");
            }
        }

        //input denominator
        test = false;
        while (test == false){
            input = new Scanner(System.in);
            test = true;

            System.out.print("Enter the denominator: ");
            try {
                denominator = input.nextFloat();
            } catch (Exception e) {
                test = false;
                System.out.println("Invalid input: not a number.");
            }
            if (denominator == 0){
                System.out.println("0 is not a valid denominator.");
                test = false;
            }
        }

        answer = division(numerator, denominator);
        System.out.print(numerator + " / " + denominator + " = " + answer);
    }
}
