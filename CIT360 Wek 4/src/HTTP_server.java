

import java.net.*;
import java.io.*;
import java.util.*;


package edu.tuckettt.json;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class HTTP_server {

    public static String getHttpContent(String string){

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            return stringBuilder.toString();

        } catch (IOException e) {
            System.err.println(e.toString());
        }
        return "Error";
    }



    public static void main(String[] args){
        System.out.println(HTTP_server.getHttpContent("http://www.google.com"));



    }
}
