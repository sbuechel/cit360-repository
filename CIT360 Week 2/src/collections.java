import java.util.*;

public class collections {

    public static void main(String[] args){
        //List
        List<String> listOfNames = new ArrayList<String>();

        listOfNames.add("Bill Johnson");
        listOfNames.add("Alfred Schlotsky");
        listOfNames.add("Valerie Williams");
        listOfNames.add("Samuel Thumps");
        listOfNames.add("Valerie Williams");

        System.out.println("List");
        System.out.println(listOfNames);

        //Queue
        Queue<String> queueOfNames = new LinkedList<String>();


        queueOfNames.add("Bill Johnson");
        queueOfNames.add("Alfred Schlotsky");
        queueOfNames.add("Valerie Williams");
        queueOfNames.add("Samuel Thumps");
        queueOfNames.add("Valerie Williams");

        System.out.println("Queue");
        System.out.println(queueOfNames);

        //Set
        Set<String> setOfNames = new HashSet<>();

        setOfNames.add("Bill Johnson");
        setOfNames.add("Alfred Schlotsky");
        setOfNames.add("Valerie Williams");
        setOfNames.add("Samuel Thumps");
        setOfNames.add("Valerie Williams");

        System.out.println("Set");
        System.out.println(setOfNames);

        //Tree
        TreeMap<String, String> mapOfNames = new TreeMap<String, String>();

        mapOfNames.put("Bill Johnson", "Value");
        mapOfNames.put("Alfred Schlotsky", "Value 1");
        mapOfNames.put("Valerie Williams", "Value 2");
        mapOfNames.put("Samuel Thumps", "Value 3");
        mapOfNames.put("Valerie Williams", "Value 4");

        System.out.println("Tree");
        System.out.println(mapOfNames);


        System.out.println("\nThe same names were added, not in alphabetical order, to the list, queue, set, and tree. " +
                "\nThe function to add the name \"Valerie Williams\" was added a second time to show that the list and " +
                "\nqueue could have the same value added multiple times and that the set and tree would only replace" +
                "\nthe data for those values.");


        System.out.println("\nThe list will now be organized in alphabetical order.");

        java.util.Collections.sort(listOfNames);

        System.out.println(listOfNames);

    }
}
